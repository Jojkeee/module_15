#include <iostream>


void func(int num, bool isEven)
{   
    for (int i = 1; i < num; i++)
    {
        std::cout << (i++ + isEven) << "\n";
    }
}

int main()
{
    int a = 15;

    for (int i = 1; i < a; i++)
    {
        if (i % 2 == 0)
        {
            std::cout << i << "\n";
        }
    }

    std::cout << "\n the end of loop \n";

    func(a, true); //������

    std::cout << "\n the end of loop \n";
    func(a, false); //��������
}

